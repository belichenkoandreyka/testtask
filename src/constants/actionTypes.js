import keyMirror from 'key-mirror/index';

export default keyMirror({
    GET_DATA: null,
    SET_DATA: null,
});
