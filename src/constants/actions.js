import actionTypes from "./actionTypes";

export const getData = () => ({ type: actionTypes.GET_DATA });
export const setData = payload => ({ type: actionTypes.SET_DATA, payload });
