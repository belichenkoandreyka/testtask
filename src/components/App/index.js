import { connect } from 'react-redux';
import App from "./App.jsx";
import * as selectors from '../../constants/selectors';
import * as actions from '../../constants/actions';


const mapStateToProps = state => ({
    data: selectors.getData(state),
});

const mapDispatchToProps = dispatch => ({
    getData: () => dispatch(actions.getData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
