import React from 'react';
import './App.css';
import { Scrollbars } from 'react-custom-scrollbars';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        props.getData()
    }

    render() {
        const data = this.props;

        const handleRedirect = (link) => {
            document.location.href = link;
        };

        return(
            <div className='app'>
                <div className='container'>
                    <Scrollbars style={{ height: 800 }}>
                    {data.data.map(item =>
                        <div className='news' onClick={handleRedirect.bind(null, item.Link)} key={item.ID}>
                            <div className='news__title'>
                                <span className='news__title_text'>{item.Title}</span>
                            </div>
                            <div className='news__description'>
                                <span className='news__description_text'>{item.Description}</span>
                            </div>
                        </div>
                    )}
                    </Scrollbars>
                </div>
            </div>
        );
    }
}

