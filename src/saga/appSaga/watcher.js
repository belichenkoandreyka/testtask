import { takeEvery } from 'redux-saga/effects';
import actionTypes from '../../constants/actionTypes.js';
import * as workers from './workers.js';

export function* watchApp() {
    yield takeEvery(actionTypes.GET_DATA, workers.getData);
}
