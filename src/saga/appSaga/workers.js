import { put } from 'redux-saga/effects';
import * as actions from '../../constants/actions.js';
import constants from "../../constants/constants.js";
import axios from 'axios';

export function* getData() {
    let data = yield axios.get(constants.link);
    yield put(actions.setData(data.data))
}
