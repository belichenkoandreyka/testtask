import { all, fork } from 'redux-saga/effects';
import { watchApp } from './appSaga/watcher.js';

const sagas = [
  watchApp
];

export default function* rootSaga() {
  yield all(sagas.map(fork));
}
