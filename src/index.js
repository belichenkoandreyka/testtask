import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/index.js';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { rootReducer } from './redux/rootReducer.js'
import createSagaMiddleware from 'redux-saga';
import rootSaga from './saga/rootSaga.js';

const saga = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(saga));
saga.run(rootSaga);

window.store = store;

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
